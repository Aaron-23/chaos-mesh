## 部署方式

通过helm方式进行部署

- 安装helm

```bash
wget https://goodrain-pkg.oss-cn-shanghai.aliyuncs.com/pkg/helm && chmod +x helm && mv helm /usr/local/bin/
```

### Step 1: 将 Chaos Mesh 存储库添加到 Helm 存储库


```bash
helm repo add chaos-mesh https://charts.chaos-mesh.org
```

添加成功后，通过以下命令搜索可用版本：


    
```bash
helm search repo chaos-mesh
```


### Step 2: 安装 




1.创建名称空间 `chaos-testing`:

```bash
kubectl create ns chaos-testing
```

2.使用 helm 安装 Chaos Mesh：

- For helm 3.X

```bash
helm install chaos-mesh chaos-mesh/chaos-mesh --namespace=chaos-testing
```

3.检查是否部署了 Chaos Mesh pod：

```bash
kubectl get pods --namespace chaos-testing -l app.kubernetes.io/instance=chaos-mesh
```

预期输出：

```bash
     NAME                                        READY   STATUS    RESTARTS   AGE
     chaos-controller-manager-6d6d95cd94-kl8gs   1/1     Running   0          3m40s
     chaos-daemon-5shkv                          1/1     Running   0          3m40s
     chaos-daemon-jpqhd                          1/1     Running   0          3m40s
     chaos-daemon-n6mfq                          1/1     Running   0          3m40s
     chaos-dashboard-d998856f6-vgrjs             1/1     Running   0          3m40s
```

### Step 3: 创建Account

项目地址：https://gitee.com/Aaron-23/chaos-mesh.git

```bash
NAME                                        READY   STATUS    RESTARTS   AGE
kubectl create -f 1-ClusterManager.yaml
kubectl create -f 2-ClusterViewer.yaml
kubectl create -f 3-NamespaceManager.yaml
kubectl create -f 4-NamespaceViewer.yaml
kubectl create -f 5-ServiceAccountcluster.yaml
kubectl create -f 6-ServiceAccountnamespace.yaml
```

#### 访问Dashboard

默认情况下，基于安全考虑使用 helm 安装 Chaos Mesh 时启用安全模式，需要使用帐户Name和登录 Chaos Dashboard Token。

- 访问Dashboard

获取svc，访问服务器`IP:31313`端口即可。

```bash
$ kubectl get svc chaos-dashboard -n chaos-testing
NAME              TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
chaos-dashboard   NodePort   10.43.225.224   <none>        2333:31313/TCP   3d
```

- 获取token

```bash
kubectl -n chaos-testing describe secret $(kubectl -n chaos-testing get secret | grep default-token | awk '{print $1}')
```

- 切换Dashboard语言
在Dashboard **设置** 中可切换语言为中文

![输入图片说明](https://images.gitee.com/uploads/images/2021/0628/100526_5c57d85b_5213691.png "屏幕截图.png")


参考文档

- https://chaos-mesh.org/docs/user_guides/installation